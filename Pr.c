#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<errno.h>
#include<time.h>
#include <sys/wait.h>


typedef struct dirent StructDIR;
typedef struct stat StructFIS;


int fisierSnap;

char caleDirIzolate[PATH_MAX];










int deschideSnapshot (char caleSnap[PATH_MAX])
{
  if((fisierSnap=open(caleSnap,O_WRONLY|O_CREAT|O_TRUNC,0644))==-1)
    {
      printf("Eroare de deschidere a Snapshot-ului %s!\n",caleSnap);
      return 0;
    }
  return 1;
}


int inchideSnapshot (char caleSnap[PATH_MAX])
{
  if(close(fisierSnap)==-1)
    {
      printf("Eroare de inchidere a Snapshot-ului %s!\n",caleSnap);
      return 0;
    }
  return 1;
}










void verificaArgumente (int nrArg, char **sirArg)
{
  if(nrArg>=16 || nrArg<=5)
    {
      printf("Numarul de argumente este gresit!\n");
      exit(EXIT_FAILURE);
    }

  if((strcmp(sirArg[1],"-o"))!=0)
    {
      printf("Lipseste argumentul -o!\n");
      exit(EXIT_FAILURE);
    }

  if((strcmp(sirArg[3],"-s"))!=0)
    {
      printf("Lipseste argumentul -s!\n");
      exit(EXIT_FAILURE);
    }
}


int verificaDirector (char caleDir[PATH_MAX])
{
  if(opendir(caleDir)==NULL)
    {
      return 0;
    }
  return 1;
}










void scrieSnapshot (char cale[PATH_MAX])
{  
  DIR *director;
  StructDIR *directorStruct;
  StructFIS directorStat;

  if((director=opendir(cale))==NULL)
    {
      printf("Directorul %s nu a fost deschis cu succes!\n",cale);
      exit(EXIT_FAILURE);
    }

  while((directorStruct=readdir(director))!=NULL)
    {
      if(strcmp(directorStruct->d_name,".")!=0 && strcmp(directorStruct->d_name,"..")!=0)
	{
	  char caleAux[PATH_MAX];
	  
	  snprintf(caleAux,PATH_MAX,"%s/%s",cale,directorStruct->d_name);

	  if(stat(caleAux,&directorStat)==-1)
	    {
	      printf("Eroare la scrierea lui %s in Snapshot!\n",caleAux);
	      exit(EXIT_FAILURE);
	    }

	  char sir[100];
	  int size;

	  size=snprintf(sir,100,"Nume: %s\n",directorStruct->d_name);
	  write(fisierSnap,sir,size);
	  
	  size=snprintf(sir,100,"Dimensiune: %ld\n",directorStat.st_size);
	  write(fisierSnap,sir,size);

	  size=snprintf(sir,100,"Modificat: %s\n\n",ctime(&directorStat.st_mtime));
	  write(fisierSnap,sir,size);
	}

      if(directorStruct->d_type==DT_DIR && strcmp(directorStruct->d_name,".")!=0 && strcmp(directorStruct->d_name,"..")!=0)
	{
	  char caleAux[PATH_MAX];

	  snprintf(caleAux,PATH_MAX,"%s/%s",cale,directorStruct->d_name);

	  scrieSnapshot(caleAux);
	}
    }
  closedir(director);
}










int compara;
void comparaDirector (char caleDirector[PATH_MAX], char caleSnapshot[PATH_MAX])
{
  StructFIS director;
  StructFIS snapshot;

  if(stat(caleDirector,&director)==-1 || stat(caleSnapshot,&snapshot)==-1)
    {
      printf("Eroare la compararea dintre directorul %s si Snapshot-ul %s, se va crea un Snapshot nou!\n",caleDirector,caleSnapshot);
      compara=1;
      return;
    }
  
  if(difftime(director.st_mtime,snapshot.st_mtime)>0)
    {
      compara=1;
      return;
    }

  DIR *directorDIR;
  StructDIR *directorStructDIR;

  if((directorDIR=opendir(caleDirector))==NULL)
    {
      printf("Eroare la deschiderea pentru comparare a directorului %s, se va crea un Snapshot nou!\n",caleDirector);
      compara=1;
      return;
    }
    
  while((directorStructDIR=readdir(directorDIR))!=NULL)
    {
      if(directorStructDIR->d_type==DT_DIR && strcmp(directorStructDIR->d_name,".")!=0 && strcmp(directorStructDIR->d_name,"..")!=0)
	{
	  char caleAux[PATH_MAX];

	  snprintf(caleAux,PATH_MAX,"%s/%s",caleDirector,directorStructDIR->d_name);

	  comparaDirector(caleAux,caleSnapshot);
	}
    }
}










int creazaProces (char *caleSnap, char *sirArg)
{
  int pid;
  pid=fork();
  
  if(pid==-1)
    {//EROARE
      printf("Eroare de creare a unui proces copil pentru scrierea in Snapshot-ul %s!\n",caleSnap);
      exit(EXIT_FAILURE);
    }
  else
    {
      if(pid==0)
	{//COPIL
	  if(deschideSnapshot(caleSnap)==0)
	    {
	      printf("Eroare de deschidere a Snapshot-ului %s!\n",caleSnap);
	      exit(EXIT_FAILURE);
	    }
	  
	  scrieSnapshot(sirArg);
	  
	  if(inchideSnapshot(caleSnap)==0)
	    {
	      printf("Eroare de inchidere a Snapshot-ului %s!\n",caleSnap);
	      exit(EXIT_FAILURE);
	    }
	  	  
	  return pid;
	}
      else
	{//PARINTE
	  return pid;
	}
    }

  return pid;
}


void incheieProcese (int vectorPID[10],int contor)
{
  int numarProces=0;
  int status;

  int i;
  for(i=0;i<contor;i++)
    {
      waitpid(vectorPID[i],&status,0);

      if(WIFEXITED(status)!=0)
	{
	  numarProces++;
	  printf("Procesul %d cu PID-ul <%d> s-a terminat cu codul <%d>!\n",numarProces,vectorPID[i],WEXITSTATUS(status));
	}
      else
	{
	  printf("Procesul cu PID-ul <%d> s-a terminat cu codul <%d> cu eroare!\n",vectorPID[i],WEXITSTATUS(status));
	  exit(EXIT_FAILURE);
	}
    }
}










int apeleazaScript (char *cale)
{
  char apel[PATH_MAX+12];
  snprintf(apel,PATH_MAX+12,"./script.sh %s",cale);

  int iesire;
  iesire=system(apel);

  return iesire;
}


void mutaFisierPericulos (char *cale, char *nume)
{
  char caleIzolat[PATH_MAX];
  snprintf(caleIzolat,PATH_MAX+1,"%s/%s",caleDirIzolate,nume);

  int eroare;
  eroare=rename(cale,caleIzolat);

  if(eroare==-1)
    {
      printf("A aparut o eroare la mutarea fisierului %s!\n",cale);
      exit(EXIT_FAILURE);
    }
  else
    {
      printf("Fisierul %s a fost mutat cu succes!\n",cale);
    }
}


void verificaPermisiuni (char *cale)
{
  DIR *director;
  StructDIR *directorStruct;
  StructFIS fisier;

  if((director=opendir(cale))==NULL)
    {
      printf("Eroare de deschidere a directorului %s pentru verificarea permisiunilor!\n",cale);
      exit(EXIT_FAILURE);
    }

  while((directorStruct=readdir(director))!=NULL)
    {
      if(directorStruct->d_type!=DT_DIR && strcmp(directorStruct->d_name,".")!=0 && strcmp(directorStruct->d_name,"..")!=0)
	{
	  char caleAux[PATH_MAX];
	  
	  snprintf(caleAux,PATH_MAX,"%s/%s",cale,directorStruct->d_name);
	  
	  if(stat(caleAux,&fisier)==-1)
	    {
	      printf("Eroare de deschidere a fisierului %s pentru verificarea permisiunilor!\n",caleAux);
	      exit(EXIT_FAILURE);
	    }

	  if((fisier.st_mode & S_IRWXU)==0 && (fisier.st_mode & S_IRWXG)==0 && (fisier.st_mode & S_IRWXO)==0)
	    {
	      printf("Fisierul %s este fara permisiuni!\n",caleAux);

	      int iesire;
	      iesire=apeleazaScript(caleAux);

	      if(iesire==-1)
		{
		  printf("Script-ul nu a fost executat cu succes!\n");
		  exit(EXIT_FAILURE);
		}

	      int iesireValoare;
	      //20-PERICULOS     10-NONPERICULOS

	      
	      if(WIFEXITED(iesire)!=0)
		{
		  iesireValoare=WEXITSTATUS(iesire);
		}

	      if (iesireValoare==20)
		{
		  mutaFisierPericulos(caleAux,directorStruct->d_name);
		}
	    }
	}
      
      if(directorStruct->d_type==DT_DIR && strcmp(directorStruct->d_name,".")!=0 && strcmp(directorStruct->d_name,"..")!=0)
	{
	  char caleAux[PATH_MAX];

	  snprintf(caleAux,PATH_MAX,"%s/%s",cale,directorStruct->d_name);

	  verificaPermisiuni(caleAux);
	}
    }
}










int main (int nrArg, char **sirArg)
{
  verificaArgumente(nrArg,sirArg);

  int vectorPID[10];
  int contor=0;

  int i;
  
  strcpy(caleDirIzolate,sirArg[4]);

  if(verificaDirector(sirArg[2])==0)
    {
      printf("Directorul %s pentru Snapshot-uri nu a putut sa fie deschis!\n",sirArg[2]);
      exit(EXIT_FAILURE);
    }

  if(verificaDirector(caleDirIzolate)==0)
    {
      printf("Directorul %s pentru fisiere izolate nu a putut sa fie deschis!\n",caleDirIzolate);
      exit(EXIT_FAILURE);
    }
  
  for(i=1;i<nrArg;i++)
    {
      if((strcmp(sirArg[i],sirArg[2])==0) && i!=2)
	{
	  printf("Numele directorului de Snapshot-uri se repeta!\n");
	  exit(EXIT_FAILURE);
	}
    }

  for(i=1;i<nrArg;i++)
    {
      if((strcmp(sirArg[i],sirArg[4])==0) && i!=4)
	{
	  printf("Numele directorului de fisiere izolate se repeta!\n");
	  exit(EXIT_FAILURE);
	}
    }
  
  for(i=5;i<nrArg;i++)
    {
      if(verificaDirector(sirArg[i])==1)
	{
	//Varianta 1, muta inainte de a face Snapshot
	  verificaPermisiuni(sirArg[i]);
	  
	  char caleSnap[PATH_MAX];
	  snprintf(caleSnap,PATH_MAX,"%s/%s",sirArg[2],sirArg[i]);

	  compara=0;
	  comparaDirector(sirArg[i],caleSnap);

	  if(compara==1)
	    {
	      vectorPID[contor]=creazaProces(caleSnap,sirArg[i]);

	      if(vectorPID[contor]==0)
		{//COPIL
		  exit(EXIT_SUCCESS);
		}
	      else
		{//PARINTE
		  printf("Snapshot-ul s-a actualizat pentru directorul %s!\n",sirArg[i]);
		  
		  contor++;
		}
	    }
	  else
	    {
	      printf("Snapshot-ul nu s-a actualizat pentru directorul %s!\n",sirArg[i]);
	    }

	  //Varianta 2, muta dupa ce face Snapshot
	  //verificaPermisiuni(sirArg[i]);
	}
      else
	{
	  printf("Argumentul %s nu este director!\n",sirArg[i]);
	}
    }

  incheieProcese(vectorPID,contor);
  
  return 0;
}
