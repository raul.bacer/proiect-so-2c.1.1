#!/bin/bash

#Verific daca se trimite exact un argument (fisierul)
if [ $# -ne 1 ]; then
    echo "Trebuie sa se trimita exact un argument! (script)"
    exit 1
fi

#Verific daca exista fisierul
if [ ! -f "$1" ]; then
    echo "Fisierul $1 nu exista! (script)"
    exit 1
fi

#Dau fisierului doar permisiune de citire strict pentru owner
chmod u+r "$1"

#Numar linii, cuvinte, caractere (din fisierul temporar)
linii=$(wc -l < "$1")
cuvinte=$(wc -w < "$1")
caractere=$(wc -c < "$1")










#TASK 5 PE PRIMA RAMURA
#TASK 4 PE A DOUA RAMURA

if [ "$linii" -lt 3 ] && [ "$cuvinte" -gt 1000 ] && [ "$caractere" -gt 2000 ]; then                   #PRIMA RAMURA -> TASK 5

    #Initializez o variabila pe nume periculos cu 10; daca gaseste elemente corupte, se va face 20
    periculos=10
    
    #Caut cuvintele periculoase
    cuvintePericuloase=("corrupted" "dangerous" "risk" "attack" "malware" "malicious")
    
    for cuvantPericulos in "${cuvintePericuloase[@]}"; do
	grep -q "$cuvantPericulos" "$1"
	if [ $? -eq 0 ]; then
	    periculos=20
	    break
	fi
    done

    #Caut caractere non-ASCII
    grep -q -P "[^\x00-\x7F]" "$1"
    if [ $? -eq 0 ]; then
	periculos=20
    fi

    #Ii iau fisierului permisiunea de citire data mai sus
    chmod 000 "$1"
    
    if [ $periculos -eq 10 ]; then
	echo "SAFE"
	exit 10 #Valoare de exit pentru fisierele ce nu vor fi izolate
    else
	echo "$1"
	exit 20 #Valoare de exit pentru fisierele ce vor fi izolate
    fi
    
else                                                                                                  #A DOUA RAMURA -> TASK 4

    #Initializez o variabila pe nume periculos cu 10; daca gaseste elemente corupte, se va face 20
    periculos=10
    
    #Caut cuvintele periculoase
    cuvintePericuloase=("corrupted" "dangerous" "risk" "attack" "malware" "malicious")
    
    for cuvantPericulos in "${cuvintePericuloase[@]}"; do
	grep -q "$cuvantPericulos" "$1"
	if [ $? -eq 0 ]; then
	    periculos=20
	    break
	fi
    done
    
    #Caut caractere non-ASCII
    grep -q -P "[^\x00-\x7F]" "$1"
    if [ $? -eq 0 ]; then
	periculos=20
    fi
    
    #Ii iau fisierului permisiunea de citire data mai sus
    chmod 000 "$1"
    
    if [ $periculos -eq 10 ]; then
	exit 10 #Valoare de exit pentru fisierele ce nu vor fi izolate
    else
	exit 20 #Valoare de exit pentru fisierele ce vor fi izolate
    fi
    
fi










#Ii iau fisierului permisiunea de citire data mai sus
chmod 000 "$1"

exit 0
